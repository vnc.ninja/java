#!/bin/sh
echo "Running NinjaVNC TOR with args: '$@'"

export DISPLAY=:0
/usr/bin/Xvfb :0 -screen 0 800x600x24 >> /tmp/Xvfb.out 2>&1 &
/usr/sbin/tor >> /tmp/tor.out 2>&1 &
java -DdisableGUI=true -jar java-1.0.0.jar $@